import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Input is a file of sales information
 * Output is multiple files containing one type of expense
 * Created by Eryka on 10/10/2015.
 */
public class P7_17 {
    public static void main(String[] args) throws FileNotFoundException {
        //Prompt for user input and output file names
        Scanner console = new Scanner(System.in);
        System.out.print("Input file: ");
        String input_filename = console.next();

        //Reading in and Writing out setup
        try {
            Scanner in = new Scanner(new File(input_filename));
            PrintWriter out_din = new PrintWriter("Dinner.txt");
            PrintWriter out_conf = new PrintWriter("Conference.txt");
            PrintWriter out_lodg = new PrintWriter("Lodging.txt");
            PrintWriter out_etc = new PrintWriter("Etc.txt");
            //Read in lines
            while (in.hasNextLine()) {
                //Array list for each line
                ArrayList<String> line_list = new ArrayList<>();

                //Take one line at a time
                String line = in.nextLine();

                //Open up a scanner on it
                Scanner line_scanner = new Scanner(line);
                //Use semicolon delimiter
                line_scanner.useDelimiter(";");

                //While there are words (separated by semicolons, add them to an arraylist for that line)
                while(line_scanner.hasNext()) {
                    line_list.add(line_scanner.next());
                }

                //Categorize the expense and write out the LINE to the correct file
                if (line_list.get(1).equals("Dinner")) {
                    out_din.println(line);
                }
                else if (line_list.get(1).equals("Conference")) {
                    out_conf.println(line);
                }
                else if (line_list.get(1).equals("Lodging")) {
                    out_lodg.println(line);
                }
                else {//all other expenses
                    out_etc.println(line);
                }

            }


            //close input and output files
            in.close();
            out_din.close();
            out_conf.close();
            out_lodg.close();
            out_etc.close();

            System.out.println("SUCCESS. Files Generated.");
        }
        catch (IOException exception) {
            System.out.print("ERROR: Could not find input file.");
        }

    }
}