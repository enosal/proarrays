import java.util.Scanner;

/**
 * Input is an integer
 * Output is true (if year is a leap year) or false (if year isn't a leap year)
 * Created by Eryka on 10/10/2015.
 */
public class P5_20 {
    public static void main(String[] args) {
        //Set up scanner & Prompt for input
        Scanner in = new Scanner(System.in);
        System.out.print("Enter year:");

        //Get year
        if (in.hasNextInt()) {
            System.out.println(isLeapYear(in.nextInt()));
        }
        else {
            System.out.println("ERROR: Input is not a valid integer");
        }


    }

    public static boolean isLeapYear(int year) {
        //No leap years before 1582
        if (year < 1582) {
            return false;
        }
        else {
            //Years divisible by 400 are leap years
            if (year % 400 == 0) {
                return true;
            }
            //Years divisible by 4 but not by 100 are leap years
            else if (year % 4 == 0 && year % 100 != 0) {
                return true;
            }
            //All other years are not leap years
            else {
                return false;
            }

        }

    }
}
