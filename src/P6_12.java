import java.util.Random;

/**
 * No input; Program generates 20 die tosses
 * Output is all runs (consecutive repeats) groups together in parantheses
 * Created by Eryka on 10/10/2015.
 */
public class P6_12 {
    public static void main(String[] args) {
        //initialize array of 20 tosses
        int[] values = new int[20];

        //Initialize random number generator
        Random rand = new Random();

        //generate array
        for (int i = 0; i < 20; i++) {
            values[i] = 1 + rand.nextInt(6);
        }

        //Find the runs
        boolean inRun = false;
        for (int i = 0; i < 20; i++){
            if (inRun) {
                if (values[i] != values[i-1]) {
                    System.out.print(")");
                    inRun = false;
                }
            }
            else {
                if (i != 19) {
                    if (values[i] == values[i+1]) {
                        System.out.print("(");
                        inRun = true;
                    }
                }
            }
            System.out.print(values[i]);
        }
        if (inRun) {System.out.print(")");}
    }

}
