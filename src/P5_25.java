import java.util.Scanner;

/**
 * Input integer zipcode
 * Output is bar code in | and :
 * Created by Eryka on 10/10/2015.
 */
public class P5_25 {
    public static void main(String[] args) {
        //Set up scanner & Prompt for input
        Scanner in = new Scanner(System.in);
        System.out.print("Enter zip code:");

        //Get barcode & print it
        if (in.hasNextInt()) {
            printBarCode(in.nextInt());
        }
        else {
            System.out.println("ERROR: Input is not a valid integer");
        }

    }

    //Digit to bar code conversion
    public static void printDigit(int d){
        if (d == 1) {
            System.out.print(":::||");
        }
        else if (d == 2) {
            System.out.print("::|:|");
        }
        else if (d == 3) {
            System.out.print("::||:");
        }
        else if (d == 4) {
            System.out.print(":|::|");
        }
        else if (d == 5) {
            System.out.print(":|:|:");
        }
        else if (d == 6) {
            System.out.print(":||::");
        }
        else if (d == 7) {
            System.out.print("|:::|");
        }
        else if (d == 8) {
            System.out.print("|::|:");
        }
        else if (d == 9) {
            System.out.print("|:|::");
        }
        else if (d == 0) {
            System.out.print("||:::");
        }
        else {
            System.out.print("Inappropriate Digit Given");
        }

    }

    public static void printBarCode(int zipCode) {
        //Initialize sum variable to 0
        int sum = 0;

        //force zipcode into a string
        String zipString = "" + zipCode;

        //Print First full line
        System.out.print("|");

        //Print Digits by indexing through them
        for (int i = 0; i <= zipString.length() - 1; i++) {
            int digit = Integer.parseInt(zipString.substring(i, i+1));
            sum += digit;
            printDigit(digit);
        }

        //Print check digit
        int check_digit = 10 - (sum % 10);
        printDigit(check_digit);

        //Print Last full line
        System.out.print("|");
    }

}
