import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * Input is a text file for reading in (test file must be in proArrays folder)
 * Output is the same text file with the line # preceding each line
 * Created by Eryka on 10/10/2015.
 */
public class P7_2 {
    public static void main(String[] args) throws FileNotFoundException {
        //Prompt for user input and output file names
        Scanner console = new Scanner(System.in);
        String input_filename = "";
        String output_filename = "";

        System.out.print("Input file: ");
        try {
            input_filename = console.next();
        }
        catch (NoSuchElementException exception) {
            System.out.println("ERROR: No input file given");
        }

        System.out.print("Output file: ");
        try {
            output_filename = console.next();
        }
        catch (NoSuchElementException exception) {
            System.out.println("ERROR: No output file specified");
        }

        try {
            //Reading in and Writing out setup
            Scanner in = new Scanner(new File(input_filename));
            PrintWriter out = new PrintWriter(output_filename);

            int line_num = 1;
            //Read in lines
            while (in.hasNextLine()) {
                out.printf("/* %d */ %s", line_num, in.nextLine() + "\r\n");
                line_num++;
            }

            //close input and output files
            in.close();
            out.close();

            System.out.println("SUCCESS!");
        }
        catch (IOException exception) {
            System.out.println("ERROR: Could not find input file or could not write output.");
        }
    }
}
