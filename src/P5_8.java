import java.util.Random;
import java.util.Scanner;

/**
 * Input is String to be parsed into separate words
 * Output is a scrambled version of all of the words (not flipping the first and last ones)
 *
 * Created by Eryka on 10/10/2015.
 */
public class P5_8 {
    public static void main(String[] args) {
        //Open input reader & prompt for input
        Scanner in = new Scanner(System.in);
        System.out.print("Enter strings:");

        //get the line of input
        String line = in.nextLine();
        //prepare line of input for scanning (delimiting on white space)
        Scanner lineScanner = new Scanner(line);

        //Send every word into the scramble method
        while (lineScanner.hasNext()) {
            System.out.println(scramble(lineScanner.next()));
        }

    }

    public static String scramble(String word) {
        //initialize empty string for scrambled word
        String scrambled = "";

        //get len of word
        int len = word.length();

        //if the word is 3 characters long or less, return the word
        if (len <= 3){
            scrambled = word;
        }

        //otherwise scramble it
        else {
            //random generator
            Random rand = new Random();

            //pick two random numbers between indices 1 and len-2.
            //characters at index 0 and len-1 must stay put
            int ran_num1 = 1 + rand.nextInt(len - 2);
            int ran_num2 = 1 + rand.nextInt(len - 2);

            //first char
            scrambled += word.charAt(0);

            //characters from 1 to len-2
            for (int i = 1; i <= len - 2; i++) {
                //if the index corresponds to the first index chosen for scrambling, replace this position with the character at the second randomly chosen index for scrambling
                if (i == ran_num1) {
                    scrambled += word.charAt(ran_num2);
                }
                else if (i == ran_num2) {
                    scrambled += word.charAt(ran_num1);
                }
                else {
                    scrambled += word.charAt(i);
                }
            }

            //last char
            scrambled += word.charAt(len-1);
        }

        return scrambled;

    }
}


