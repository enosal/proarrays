import java.lang.*;
import java.util.*;


/**
 * Input is
 *      a line to be parsed into strings for an array list of strings,
 *      a line to be parsed into integers for an array list of integers
 * Output is a histogram with captions
 * Created by Eryka on 10/10/2015.
 */
public class P6_23 {
    public static void main(String[] args) {
        //initialize array lists for values and captions
        ArrayList<Integer> values = new ArrayList<>();
        ArrayList<String> captions = new ArrayList<>();

        //Initialize scanner
        Scanner in = new Scanner(System.in);

        //Get captions
        System.out.print("Enter Captions:");
        String line1 = in.nextLine();
        Scanner cap_scanner = new Scanner(line1);

        //Get data values
        System.out.print("Enter Data Values:");
        String line2 = in.nextLine();
        Scanner val_scanner = new Scanner(line2);

        //Put captions in a string array
        while (cap_scanner.hasNext()) {
            captions.add(cap_scanner.next());
        }

        //Put values in an integer array
        while (val_scanner.hasNextInt()) {
                values.add(val_scanner.nextInt());
            }


        //Compare sizes of arrays
        if (values.size() != captions.size()) {
            System.out.println("ERROR: Mismatch between captions and data points entered.");
        }
        else {
            //get histogram
            histogram(captions, values);
        }
    }


    public static void histogram(ArrayList<String> caps, ArrayList<Integer> vals) {
        //Initialize size and max variables
        int size = vals.size();
        double max = -1.0;

        //find max of values
        for (int val : vals) {
            if (val > max) {
                max = val;
            }
        }

        //Print out asterisks
        for (int i = 0; i < size; i++) {
            System.out.printf("%20s", caps.get(i) + " ");
            for (int j = 1; (j <= vals.get(i)/max * 40) ; j++) {
                System.out.printf("%-1s", "*");
            }
            System.out.println("");
        }


    }



}
