import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * No explicit input; create two arrays in main and call "mergeSorted" on it
 * Output is a merged array list of the two input array lists
 * Created by Eryka on 10/10/2015.
 */
public class P6_28 {
    public static void main(String[] args) {
        ArrayList<Integer> first = new ArrayList<>();
        ArrayList<Integer> second = new ArrayList<>();

        //CHANGE THESE ARRAYS!
        int[] first_list = {1, 3, 10, 24};
        int[] second_list = {1, 2, 3, 5, 9, 10, 10, 11, 25};

        System.out.println("First sorted array: " + Arrays.toString(first_list));
        System.out.println("Second sorted array: " + Arrays.toString(second_list));

        //Put arrays into array lists
        for (int f : first_list) {
            first.add(f);
        }
        for (int s : second_list) {
            second.add(s);
        }


        System.out.println("Merged array list: " + mergeSorted(first, second));

    }


    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b) {
        //merged will hold the sorted merged array
        ArrayList<Integer> merged = new ArrayList<>();

        //array indices for a and b;
        int aIn = 0;
        int bIn = 0;

        //While neither fo the ends of the arrays have been reached, add the smallest values to merged
        while (aIn < a.size() && bIn < b.size()){
            if (a.get(aIn) < b.get(bIn)) {
                merged.add(a.get(aIn));
                aIn++;
            }
            else {
                merged.add(b.get(bIn));
                bIn++;
            }
        }
        //if the end of a has been reached, copy over the last values of b into merged
        if (aIn == a.size()) {
            for (int bI = bIn; bI < b.size(); bI++) {
                merged.add(b.get(bI));
            }
        }
        //otherwise, if the end of b has been reached, copy over the last values of a into merged
        else if (bIn == b.size()) {
            for (int aI = aIn; aI < a.size(); aI++) {
                merged.add(a.get(aI));
            }
        }

        return merged;

    }
}
